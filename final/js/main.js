var app = new Vue({
    el: '#app',
    mounted(){
        this.allTasks();
    },
    data: {
      message: 'Bienvenidos al Workshop 4Geeks de VueJS!',
      newtask: '',
      tasks: [],
    },
    methods: {
        allTasks(){
            axios.get('http://localhost:8000/api/tasks').then(response => {
                console.log(response.data.message);
                this.tasks = response.data.tasks;
            }).catch(error => {
                console.log(error);
            });
        },
        addTask(){
            axios.post('http://localhost:8000/api/tasks', { "task": this.newtask, "completed": "no" }).then(response => {
                alert(response.data.message);
                console.log(response.data.message);
                this.newtask = '';
                this.allTasks();
            }).catch(error => {
                console.log(error);
            });
        },
        completeTask(task){
            axios.put('http://localhost:8000/api/tasks/' + task.id, { "task": task.task, "completed": (task.completed == "no" ? "si" : "no") }).then(response => {
                alert(response.data.message);
                console.log(response.data.message);
                this.allTasks();
            }).catch(error => {
                console.log(error);
            });
        },
        deleteTask(task){
            axios.delete('http://localhost:8000/api/tasks/' + task.id).then(response => {
                alert(response.data.message);
                console.log(response.data.message);
                this.allTasks();
            }).catch(error => {
                console.log(error);
            });
        }
    }
})

